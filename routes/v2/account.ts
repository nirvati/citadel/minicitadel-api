import { Router } from "https://deno.land/x/oak@v11.1.0/mod.ts";

import * as auth from "../../middlewares/auth.ts";
import * as nirvati from "../../logic/nirvati.ts";
import * as typeHelper from "../../utils/types.ts";
import { getPasswordFromContext } from "../../utils/auth.ts";

const router = new Router({
  prefix: "/v2/account",
});


router.post(
  "/login",
  async (ctx, next) => {
    const plainTextPassword = await getPasswordFromContext(ctx);
    typeHelper.isString(plainTextPassword, ctx);
    const jwt = await nirvati.getJwt(plainTextPassword);

    ctx.response.body = { jwt };
    await next();
  },
);

router.get("/info", auth.jwt, async (ctx, next) => {
  ctx.response.body = { name: "miniCitadel" };
  await next();
});

router.post("/refresh", async (ctx, next) => {
  const reqJwt = ctx.request.headers.get("Authorization")?.split(" ")[1];
  const jwt = await nirvati.refreshJwt(reqJwt!);
  ctx.response.body = { jwt };
  await next();
});

export default router;
