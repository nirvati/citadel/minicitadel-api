import { Router } from "https://deno.land/x/oak@v11.1.0/mod.ts";

const router = new Router({
  prefix: "/ping",
});

router.get("/", async (ctx, next) => {
  ctx.response.body = {
    version: "Minicitadel by Nirvati",
  };
  await next();
});

export default router;
