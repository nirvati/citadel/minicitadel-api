export default {
  get DEVICE_HOSTNAME() {
    return Deno.env.get("DEVICE_HOSTNAME") || "citadel.local";
  },
  get JWT_PUBLIC_KEY_FILE() {
    return Deno.env.get("JWT_PUBLIC_KEY_FILE") || "/db/jwt-public-key/jwt.pem";
  },
  get ELECTRUM_HOST() {
    return Deno.env.get("ELECTRUM_HOST");
  },
  get ELECTRUM_PORT() {
    return Deno.env.get("ELECTRUM_PORT")
      ? Number.parseInt(Deno.env.get("ELECTRUM_PORT") as string, 10)
      : undefined;
  },
  get BITCOIN_P2P_PORT() {
    return Deno.env.get("BITCOIN_P2P_PORT")
      ? Number.parseInt(Deno.env.get("BITCOIN_P2P_PORT") as string, 10)
      : undefined;
  },
  get BITCOIN_RPC_PORT() {
    return Deno.env.get("BITCOIN_RPC_PORT")
      ? Number.parseInt(Deno.env.get("BITCOIN_RPC_PORT") as string, 10)
      : undefined;
  },
  get BITCOIN_RPC_USER() {
    return Deno.env.get("BITCOIN_RPC_USER");
  },
  get BITCOIN_RPC_PASSWORD() {
    return Deno.env.get("BITCOIN_RPC_PASSWORD");
  },
  get BITCOIN_HOST() {
    return Deno.env.get("BITCOIN_HOST");
  },
  get TOR_PROXY_IP() {
    return Deno.env.get("TOR_PROXY_IP") || "192.168.0.1";
  },
  get TOR_PROXY_PORT() {
    return Number.parseInt(Deno.env.get("TOR_PROXY_PORT") || "9050", 10) ||
      9050;
  },
  get BITCOIN_P2P_ONION_URL() {
    return Deno.env.get("BITCOIN_P2P_ONION_URL")
  },
  get BITCOIN_RPC_ONION_URL() {
    return Deno.env.get("BITCOIN_RPC_ONION_URL")
  },
  get ELECTRUM_ONION_URL() {
    return Deno.env.get("ELECTRUM_ONION_URL")
  },
  get NIRVATI_API() {
    return Deno.env.get("NIRVATI_API")
  },
  get SERVICES_YML() {
    return Deno.env.get("SERVICES_YML") || "/citadel-services/installed.yml";
  }
};
