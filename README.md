# Minicitadel API

This is the main backend of Minicitadel that should be deprecated in favour of the new API we're building.

---

## 🙏 Acknowledgements

The Citadel API is inspired by and built upon the work done by
[Umbrel](https://github.com/getumbrel) on its open-source
[Manager API](https://github.com/getumbrel/umbrel-manager).

The original code we forked is licensed under

```
Copyright (c) 2018-2019 Casa, Inc. https://keys.casa/
Copyright (c) 2020 Umbrel. https://getumbrel.com/
```
