import { isValidJwt } from "../logic/nirvati.ts";
import { isString } from "../utils/types.ts";
import { Middleware, Status } from "https://deno.land/x/oak@v11.1.0/mod.ts";


export const jwt: Middleware = async (ctx, next): Promise<void> => {
  const reqJwt = ctx.request.headers.get("Authorization")?.split(" ")[1];
  if (!reqJwt) {
    ctx.throw(Status.BadRequest, "Missing authorization header");
  }
  isString(reqJwt, ctx);
  const isValid = await isValidJwt(
    reqJwt as string,
  );
  if (!isValid) {
    ctx.throw(Status.Unauthorized, "Invalid JWT");
  }
  await next();
};
