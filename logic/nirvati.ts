import { request, gql } from "npm:graphql-request";
import constants from "../utils/const.ts";

const doLogin = gql`
  mutation doLogin($password: String!) {
    login(username: "admin", password: $password)
  }
`;

const refreshJwtQuery = gql`
  query getJwt {
    users {
      me {
        jwt
      }
    }
  }
`;

export async function getJwt(password: string): Promise<string> {
  return (
    await request<{ login: string }>(constants.NIRVATI_API!, doLogin, {
      password,
    })
  ).login;
}

export async function refreshJwt(jwt: string): Promise<string> {
  return (
    await request<{ users: { me: { jwt: string } } }>({
      url: constants.NIRVATI_API!,
      document: refreshJwtQuery,
      requestHeaders: {
        Authorization: `Bearer ${jwt}`,
      },
    })
  ).users.me.jwt;
}

export async function isValidJwt(jwt: string): Promise<boolean> {
  try {
    await request<{ users: { me: { jwt: string } } }>({
      url: constants.NIRVATI_API!,
      document: refreshJwtQuery,
      requestHeaders: {
        Authorization: `Bearer ${jwt}`,
      },
    });
    return true;
  } catch {
    return false;
  }
}
