import constants from "../utils/const.ts";

export type ConnectionDetails = {
  address: string;
  port: number;
  connectionString: string;
};

export type RpcConnectionDetails = ConnectionDetails & {
  rpcuser: string;
  rpcpassword: string;
};

export function getElectrumConnectionDetails(): ConnectionDetails {
  try {
    const addressUnformatted = constants.ELECTRUM_ONION_URL!;
    const address = addressUnformatted.trim();
    const port = constants.ELECTRUM_PORT!;
    const connectionString = `${address}:${port}:t`;
    return {
      address,
      port,
      connectionString,
    };
  } catch {
    throw new Error("Unable to get Electrum hidden service url");
  }
}

export function getBitcoinP2pConnectionDetails(): ConnectionDetails {
  try {
    const address = constants.BITCOIN_P2P_ONION_URL!;
    const port = constants.BITCOIN_P2P_PORT!;
    const connectionString = `${address}:${port}`;
    return {
      address,
      port,
      connectionString,
    };
  } catch {
    throw new Error("Unable to get Bitcoin P2P hidden service url");
  }
}

export function getBitcoinRpcConnectionDetails(): RpcConnectionDetails {
  try {
    const label = encodeURIComponent(`Your Citadel`);
    const rpcuser = constants.BITCOIN_RPC_USER!;
    const rpcpassword = constants.BITCOIN_RPC_PASSWORD!;
    const address = constants.BITCOIN_RPC_ONION_URL!;
    const port = constants.BITCOIN_RPC_PORT!;
    const connectionString = `btcrpc://${rpcuser}:${rpcpassword}@${address}:${port}?label=${label}`;
    return {
      rpcuser,
      rpcpassword,
      address,
      port,
      connectionString,
    };
  } catch {
    throw new Error("Unable to get Bitcoin RPC connection details");
  }
}
